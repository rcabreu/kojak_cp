# Búsqueda Completa

También conocida como "Fuerza Bruta", es un paradigma de resolución de problemas que consiste en explorar a **totalidad** el espacio de soluciones del problema dado para encontrar la mejor solución.

Salvo en el caso de que la implementación tenga errores, este paradigma garantiza que la solución encontrada es siempre la correcta. Por tanto, si el algoritmo trabaja por debajo de los límites de memoria y tiempo, casi siempre lo correcto es implementarlo, reclamar los puntos y utilizar el tiempo que de otra manera estarías dedicando a encontrar un algoritmo más sofisticado para otros problemas o a sub-tareas más valiosas.

Incluso cuando la solución basada en este paradigma para un problema es **prohibitivamente lenta**, la misma puede resultar útil y contribuir en la confección de otra solución correcta y más eficiente. Por ejemplo, una solución basada en búsqueda completa puede servir de **verificador** de las salidas de otra solución para casos pequeños. En otras situaciones tenemos problemas que exhiben patrones observables entre las entradas y sus salidas. Las soluciones basadas en búsqueda completa pueden servir para poder recrear el patrón, para luego estudiarlo y, con suerte, escribir una solución que lo explote.

## Poda (Pruning)

A veces no es necesario revisar el espacio de soluciones completo de un problema. Esto es porque a veces se sabe o se puede determinar que algunas soluciones candidatas son inválidas o innecesarias. Por ejemplo, si queremos determinar la edad de una persona, no es necesario revisar más allá de $150$, por un factor biológico. La poda, o pruning, como mejor se conoce, encarna este concepto.

## Problemas iterativos para practicar

- [USACO - Milk Pails](https://www.acmicpc.net/problem/11999)
- [USACO - Diamond Collector](https://www.acmicpc.net/problem/12005)
- [USACO - Daisy Chains](https://www.acmicpc.net/problem/20651)
- [UVa 725 - Division](https://onlinejudge.org/index.php?option=onlinejudge&Itemid=8&page=show_problem&problem=666)

## Generación de Subconjuntos

Para algunos problemas, la solución óptima está dictada por algún subconjunto de un conjunto de elementos dada.
En estos problemas, el espacio de soluciones está conformado por todos los subconjuntos de algún conjunto. La solución
óptima es poder elegir el mejor subconjunto.

Nota que para un conjunto de tamaño $n$, existen exactamente $2^n$ subconjuntos posibles (incluyendo el sub-conjunto vacío).
Cualquier algoritmo de orden exponencial no puede trabajar rápidamente con $n$ grandes. De hecho, el algoritmo debajo, para
recorrer por todos los sub-conjuntos, tiene una complejidad de $O(2^n \times n)$, por lo que cuando $n$ se aproxima a $20$, 
ya posiblemente no sea lo suficientemente rápido en un juez en línea. 

### Preliminar - máscaras de bit

Una máscara de bit es simplemente la representación binaria de alguna cantidad. Dicho de otra manera, una máscara de bit
representa una suma de algunas potencias de $2$. Por ejemplo, el número $18$ es la suma de $2^4 + 2^1$. En notación binaria,
esta magnitud se expresa como $0b1_40_30_21_10_0$. Aquí, "$0b$" solo indica que el número está en base 2 y, si el contexto
es claro sobre de qué se está hablando, usualmente no se incluye. El dígito en la posición $4$, que es el más a la izquierda, 
se conoce como el "bit más significativo". Nota que el índice de este bit es $4$ para $18$. Asimismo, el bit en la posición
$0$ se conoce como el "bit menos significativo". Cualquier entero puede representarse en esta base.

Nuestras computadoras digitales modernas trabajan en el sistema binario. Estas pueden trabajar con enteros de $64, 32, 16$ y $8$ bits.
Esto significa que ellas tienen circuitos especializados en operar con estos enteros.

Como deben de trabajar con valores positivos y negativos, casi todas las computadoras modernas implementan el método de 
[Complemento a Dos](https://en.wikipedia.org/wiki/Two%27s_complement). Está fuera de estas notas discutirlo, pero lo más importante
para lo que sigue es que el bit más significativo utilizado para representar alguna magnitud es $n - 1$, donde $n$ es la cantidad de bit de un tipo de datos.
Por ejemplo, para $int$, que es de 32 bits, se utiliza del bit $0$ al bit $30$ (inclusive). En gran parte, por esto es que *int* solo almacena
hasta $2^{31} - 1 = 2147483647$.

En C++, el operador binario "<<" (por ejemplo, "a << b"), conocido como *left shift*, se utiliza para desplazar la máscara de bits de $a$ $b$ posiciones hacia la izquierda.
Cuando $a = 1$, esa operación es igual a $2^b$. Notar para este último caso:

- Todos los números entre $0$ y $2^b - 1$ utilizan hasta $b - 1$ bits.

En C++, el operador binario "&" (por ejemplo, "a & b") aplica, bit a bit, el operador lógico AND para las máscaras $a$ y $b$. Si nos interesa ver
si algún bit está encendido o no, para una máscara $a$, podemos hacer "a & (1 << x)", donde $x$ es el índice del bit de interés. 

## Algoritmo basado en máscaras de bit para la generación de subconjuntos

Para generar los subconjuntos de algún conjunto de $n$ elementos, podemos hacer algo como:

```cpp
for (int b = 0; b < (1 << n); ++b) {
    vector<int> subconjunto;
    for (int j = 0; j < n; ++j) {
        if (b & (1 << j)) {
            subconjunto.push_back(j);
        }
    }
    // hacer algo con subconjunto
}
```

### Algoritmo recursivo para la generación de Subconjuntos

```cpp
void go(int n, int idx, vector<int>& actual) {
    if (idx == n) {
        // hacer algo con el subconjunto "actual"
        return;
    }

    // añade A_idx
    actual.push_back(idx);
    go(n, idx + 1, actual);

    // no añadas el A_idx
    actual.pop_back();
    go(n, idx + 1, actual);
}
```

### Problemas de generación de subconjuntos para practicar
- [AtCoder - Train Ticket](https://atcoder.jp/contests/abc079/tasks/abc079_c)
- [CodeForces - Preparing Olympiad](https://codeforces.com/problemset/problem/550/B)
- [CSES - Apple Division](https://cses.fi/problemset/task/1623)

## Todas las permutaciones

Algunos problemas nos piden encontrar la [permutación](https://en.wikipedia.org/wiki/Permutation) de un conjunto de elementos que mejor satisface algún criterio de optimalidad.
Recordando que una permutación es un reordenamiento de elementos siguiendo un orden lineal, existen $n * (n - 1) * (n - 2) * \ldots * 1 = n!$ posibles permutaciones para un
conjunto de $n$ elementos. La función *next_permutation()* de *C++* nos ayuda a recorrer fácilmente por cada una de ellas.

### Algoritmo iterativo para la generación de permutaciones:

```cpp
  vector<int> elems(n);
  for (int i = 0; i < n; i++) elems[i] = i;

  do {
    // evaluar la permutación actual de acuerdo a los requisitos del problema

  } while (next_permutation(elems.begin(), elems.end()));
```

La complejidad del bloque de arriba (considerando la lógica de evaluación siendo O(n)) es del orden $O(n \times n!)$. Esto significa que cuando $n$ se aproxima a $10$, estamos llegando a los límites de factibilidad de esta solución. Esto no necesariamente es algo malo: algunos problemas **solo** se pueden resolver de esta forma. Un *hint* de que esta es la solución requerida es observar el tamaño de $n$. Si es cerca de la vecindad de $10$, posiblemente sea la correcta. Si $n$ es mucho más grande, **NO** lo es. Sin embargo, para problemas de la **IOI**, posiblemente existan sub-tareas donde la solución esperada sea esta. En este caso, la estrategia básica nos dice que debemos escribirla y reclamar tantos puntos podamos. De hecho, esta solución luego pudiera estudiarse para detectar algún patrón o simplemente como validadora de los resultados de otra solución con mejor tiempo de ejecución.

### Algoritmo recursivo para la generación de permutaciones

```cpp
void go(vector<int>& actual) {
    if (actual.size() == n) {
        // evaluar la permutación actual de acuerdo a los requisitos del problema
        return;
    }

    for (int i = 0; i < n; ++i) {
        if (used[i]) {
            continue;
        }
        used[i] = true;
        actual.push_back(i);
        go(actual);
        used[i] = false;
        actual.pop_back();
    }
}
```

La complejiad de este algoritmo es similar a su homólogo iterativo.

### Problemas a practicar el tema de generación de permutaciones

- [UVa 11742 - Social Constraints](https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=2842)
- [AtCoder - One More aab aba baa](https://atcoder.jp/contests/abc215/tasks/abc215_c)


## Backtracking (retroceso)

Es un tipo de algoritmo recursivo que, iniciando desde una solución candidata vacía, la va extendiendo paso a paso hasta completarla,
y retrocede (abandona) una solución en cuanto se tenga suficiente información que es inválida. Lo anterior implica que esta técnica
solo se puede utilizar en problemas que admiten "soluciones parciales candidatas". Muchos juegos, como Sudoku y Crucigrama exhiben esta
propiedad. Esa propiedad funge como la poda.


![Ocho reinas](8queens.png)

```cpp
const int N = 8;
void go(int row) {
    if (row == N) {
        // hemos construido una configuración válida
        // hacer algo con ella
        return;
    }

    for (int col = 0; col < N; ++col) {
        if (ok(row, col)) {
            placeQueen(row, col);
            go(row + 1);
            removeQueen(row, col);
        }
    }
}
```

Fíjate que la versión más fuerza bruta debe hacer operaciones en el orden de $\binom{64}{8}$, o más o menos $4 \times 10^9$ , lo 
cual es demasiado lento. Esta versión, basada en backtracking solo hace operaciones en el orden de $8!$, o más o menos $4 \times 10^4$, 
que es bastante rápido.

### Problemas a practicar backtracking
- [CSES - Chessboard and Queens](https://cses.fi/problemset/task/1624)
- [USACO - Air Cownditioning II](https://www.acmicpc.net/problem/27562)
- [USACO - Livestock Lineup](https://www.acmicpc.net/problem/18270)


