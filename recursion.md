# Recursión

La [Recursión](https://en.wikipedia.org/wiki/Recursion) es un proceso de definir la solución de un problema en términos de versiones más sencillas de ella misma.
Imagínate que a lo largo de una calle se encuentran muchas casas y te encuentras en frente de una de ellas, tal y como se muestra en la imagen:

![Cuántas casas?](lineofhouses.jpg)

¿Cuántas casas hay entre la casa en la que te encuentras y aquella que tiene una estrella? La manera tradicional de resolver este problema es ir casa por casa contando
hasta que llegues a la casa deseada. Sin embargo, la respuesta también se puede expresar de la siguiente manera:

1. Si ya me encuentro en la casa deseada, paro.
2. Tomo un paso en la dirección deseada, y se lo añado a lo que sería la respuesta si iniciara desde la casa hacia donde me moví.

De manera simbólica, sea $f(n)$ una función que da la respuesta al problema en función de $n$: el índice de la casa donde te encuentras. $f(n)$ se puede definir de la siguiente manera:
$f(n) = 1 + f(n + 1)$. Claro, esta función es incompleta porque no está aún limitada. Así que debemos definirla por trozos e incluir lo siguiente: $f(n) = 0 \text{ si } n = casa\_deseada$.

En este ejemplo, $f(n + 1)$ es una versión más "sencilla" que $f(n)$ porque $n + 1$ está más cerca de $M$ (posición de la casa con la estrella).

Si este concepto se te parece a la inducción matemática, ¡estás en lo correcto! Los términos se utilizan de manera intercambiable en matemática, sin embargo, es bueno mencionar que 
estrictamente hablando no son lo mismo. La inducción matemática se utiliza para **probar** algún resultado, teorema o enunciado. En cambio, la **recursión** se utiliza para resolver
problemas que pueden descomponerse en versiones más sencillas de sí mismos. Dicho esto, las reglas generales de ambas son bastante parecidas, y conocimiento de una puede ayudar significativamente
a desarrollar habilidades en la otra.

## Partes de la recursión

Todas las funciones recursivas deben tener las siguientes partes:

1. Caso base. Es el caso que es tan sencillo que puede resolverse sin recurrir. De otras palabras, es la indicación de **cuándo parar**.
2. Trabajo. Lo que debe hacerse en cada llamada recursiva. Usualmente esto es la pega que conecta las llamadas recursivas entre sí. En el ejemplo anterior, el trabajo sería "sumar $1$ al resultado
de la próxima llamada recursiva.
3. Caso recursivo. Es lo que descompone el problema actual en otro(s) más sencillos.

## Ejemplos

### Factorial de un número

El factorial de un entero no negativo, $n$, es el producto de todos los números enteros positivos menores o iguales a $n$.
Por ejemplo, $5! = 5 \times 4 \times 3 \ times 2 \ times 1 = 120$. Por convención, $0! = 1$.

Una solución recursiva al problema de encontrar el $n-ésimo$ factorial es la siguiente:

```cpp
int f(int n) {
    // caso base
    if (n == 0) {
        return 1;
    }

    // caso recursivo
    int otro = f(n - 1);

    // trabajo
    return n * otro;
}
```

### Fibonacci

La secuencia de Fibonacci es aquella dada por estos números: $0, 1, 1, 2, 3, 5, 8, 13, \ldots$. Esta secuencia está definida por la siguiente relación de recurrencia:
$F_n = F_{n - 1} + F_{n - 2}$, con los casos bases $F_0 = 0$ y $F_1 = 1$. Encontrar el n-ésimo Fibonacci, de manera recursiva, se puede ver de esta forma:

```cpp
int f(int n) {
    if (n == 0 || n == 1) {
        return n;
    }

    return f(n - 1) + f(n - 2);
}
```

## Complejidad de los algoritmos recursivos
La complejidad de los algoritmos recursivos se determina calculando la cantidad de trabajo que se hace en cada llamada recursiva,
y la cantidad de llamadas recursivas. La clave es visualizar el árbol de recursión. Por ejemplo, para el problema del Factorial, tenemos:

![Factorial call tree](image.png)

Es fácil ver que para $f(n)$, el número de llamadas recursivas que se crean es del orden de $O(n)$. Asimismo, el trabajo de cada nivel es solo de multiplicar.
Así, el tiempo de ejecución para factorial es $n \times 1 = O(n)$ peor caso.

El caso de Fibonacci es un poco más complejo. El árbol de recursión se ve de la siguiente manera:

![Fibonaccic all tree](image-1.png)

Se ve que para el primer nivel, hay $1$ instancia. Para el segundo nivel, hay $2$, luego $4$, luego $8$, etc. Claro, el árbol que se forma no es completo: algunos 
caminos terminan antes que otros puesto que ellos llegan a los casos bases antes que los demás. Sin embargo, queda claro que el algoritmo exhibe un comportamiento
exponencial, y que $O(2^n)$ es una cota superior sobre la cantidad de llamadas recursivas, si bien esta no sea la más fuerte. Considerando que cada llamada hace
$O(1)$ trabajo (esencialmente solo hace una suma), el tiempo de Fibonacci recursivo se considera $O(2^n)$.

En medio de una competencia, es impráctico determinar la base exacta. Si al menos llegas a determinar que es exponencial, ya tienes suficiente información: el algoritmo
es sumamente ineficiente para valores relativamente pequeños de $n$. Si te interesa saber, el algoritmo en el peor caso es aproximadamente [$O(1.61^n)$](http://www.blog.republicofmath.com/how-fast-do-the-fibonacci-numbers-grow/).

